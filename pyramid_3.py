number = int(input())
space = number
index = 1
starList = []
for star in range(number):
    print(end=" "*(space-1))
    print("*"*index)
    if star != (number-1):
        starList.append("*"*index)
    space = (space-1)
    index = index + 2
starList.reverse()
for star in starList:
    print(end=" "*(space+1))
    space = (space+1)
    print(star)