import string
password = str(input())
specialCharactersSet = set(string.punctuation)
length = len(password)
if length >= 3 and length <= 20:
    checkList = {
        "upper": False,
        "lower": False,
        "number": False,
        "specialCharacters": False
    }
    checkList["upper"] = any(char.isupper() for char in password)
    checkList["lower"] = any(char.islower() for char in password)
    checkList["number"] = any(char.isnumeric() for char in password)
    checkList["specialCharacters"] = any(char in specialCharactersSet for char in password)