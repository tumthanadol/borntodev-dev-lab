n = int(input())

if n > 0 and n < 100:
    for sharp in range(0,n):
        if sharp == 0 or sharp == (n-1):
            print("#"*n)
        else:
            print("#",end=" "*(n-2))
            print("#")